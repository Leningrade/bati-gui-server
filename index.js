const express = require('express')
const morgan = require('morgan')
const { createProxyMiddleware } = require('http-proxy-middleware')
const cors = require('cors')
const secrets = require('./secrets.js')

const heroURL = 'https://superheroapi.com/api.php/' + secrets.API_KEY

// create server

const app = express()

const PORT = 3001
HOST = 'localhost'
API_SERVICE_URL = heroURL

//setup

app.use(morgan('dev'))
app.use(cors())

//test endpoint

app.get('/info', (req, res, next) => {
  res.send(
    'Simple proxy service to envelop requests meant for superheroAPI (due to lack of cors restrictions ....)'
  )
})

app.use(
  '/API',
  createProxyMiddleware({
    target: API_SERVICE_URL,
    changeOrigin: true,
    pathRewrite: {
      [`^/API`]: '',
    },
  })
)

app.listen(PORT, HOST, () => {
  console.log(`Starting Proxy server at ${HOST}:${PORT}`)
})
